// Chair Pins
// |2 1|
// |4 3 
// |6 5 
// |8 7|

// 1 = 5V
// 2 = Left_Right 1
// 3 = 0V
// 4 = Fore_Aft 1
// 5 = Fore_Aft 2
// 6 = Center
// 7 = Left_Right 2
// 8 = Not connected

int Reference_PWM  = 9;
int Fore_Aft_PWM   = 10;
int Left_Right_PWM = 11;

int JazzyNeutral = 250; // Volts * 100
int JazzyLower   = 111; // Volts * 100
int JazzyUpper   = 389; // Volts * 100

int PWMLower = 0;
int PWMUpper = 255;

int VoltsLower = 0;
int VoltsUpper = 500;

void setup() {
  Neutral(JazzyNeutral);
  delay(5000); // Allow time to turn on the control stalk
}

void loop() {
  // Forward
  Fore_Aft(JazzyUpper);
  delay(5000);
  
  // Stop
  Fore_Aft(JazzyNeutral);
  delay(5000);
  
  // Reverse
  Fore_Aft(JazzyLower);
  delay(5000);
  
  // Stop
  Fore_Aft(JazzyNeutral);
  delay(5000);
  
  // Right
  Left_Right(JazzyUpper);
  delay(5000);
  
  // Stop
  Left_Right(JazzyNeutral);
  delay(5000);
  
  // Left
  Left_Right(JazzyLower);
  delay(5000);
  
  // Stop
  Left_Right(JazzyNeutral);
  delay(5000);
}

void Neutral(int Volts){
  Reference(Volts);
  Fore_Aft(Volts);
  Left_Right(Volts);
}

void Reference(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Reference_PWM, Pulses);
}

void Fore_Aft(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Fore_Aft_PWM, Pulses);
}

void Left_Right(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Left_Right_PWM, Pulses);
}
