// De 0 à 90 gauche à 100
// de 90 à 180 gauche à -100
// de 180 à 270 de -100 à 0
// à 270 gauche à -100
// de 270 à 360 de 0 à 100

int angle = 0;
String sangle;
int UpDownOutput;
int LeftRightOutput;

int Reference_PWM  = 9;
int Fore_Aft_PWM   = 10;
int Left_Right_PWM = 11;

int JazzyNeutral = 250; // Volts * 100
int JazzyLower   = 111; // Volts * 100
int JazzyUpper   = 389; // Volts * 100

int PWMLower = 0;
int PWMUpper = 255;

int VoltsLower = 0;
int VoltsUpper = 500;

void convertForMotors(int input){
  if (input == 0){
    UpDownOutput = 389;
    LeftRightOutput = 250;
  } else if (input == 45){
    UpDownOutput = 389;
    LeftRightOutput = 389;
  } else if (input == 90){
    UpDownOutput = 250;
    LeftRightOutput = 389;
  } else if (input == 135){
    UpDownOutput = 111;
    LeftRightOutput = 111;
  } else if (input == 180){
    UpDownOutput = 111;
    LeftRightOutput = 250;
  } else if (input == 225){
    UpDownOutput = 111;
    LeftRightOutput = 111;
  } else if (input == 270){
    UpDownOutput = 250;
    LeftRightOutput = 111;
  } else if (input == 315){
    UpDownOutput = 389;
    LeftRightOutput = 111;
  } else {
    UpDownOutput = 250;
    LeftRightOutput = 250;
  }
}

void Neutral(int Volts){
  Reference(Volts);
  Fore_Aft(Volts);
  Left_Right(Volts);
}

void Reference(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Reference_PWM, Pulses);
}

void Fore_Aft(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Fore_Aft_PWM, Pulses);
}

void Left_Right(int Volts){
  int Pulses = map(Volts, VoltsLower, VoltsUpper, PWMLower, PWMUpper);
  analogWrite(Left_Right_PWM, Pulses);
}

void setup() {
  Serial.begin(9600);
  Neutral(JazzyNeutral);
  delay(5000);
}

void loop() {
  if(Serial.available() > 0){
    sangle = Serial.readStringUntil('\n');
    angle = sangle.toInt();
    Serial.flush();
    convertForMotors(angle);
  }
  Fore_Aft(UpDownOutput);
  Left_Right(LeftRightOutput);
  
} 
