// Adresse mac de la télécommande : 24:62:AB:D4:C1:68

#define pin1 4
#define pin2 2
#define pin3 15
#define pin4 13
#define pin5 12
#define pin6 14
#define pin7 27
#define pin8 33
#define pin9 32

#include <esp_now.h>
#include <WiFi.h>

uint8_t broadcastAddress[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

typedef struct struct_message {
  int a;
} struct_message;

struct_message myData;

uint8_t theAnalogPins[9] = {pin1, pin2, pin3, pin4, pin5, pin6, pin7, pin8, pin9};
byte pinValues[9] = {120, 120, 120, 120, 120, 120, 120, 120, 120};
byte newpinValues[9] = {120, 120, 120, 120, 120, 120, 120, 120, 120};
byte newerpinValues[9] = {120, 120, 120, 120, 120, 120, 120, 120, 120};
byte averages[9] = {120, 120, 120, 120, 120, 120, 120, 120, 120};
bool state[9] = {LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW};
byte seuils[9] = {40, 70, 50, 60, 50, 80, 50, 80, 50};
int angle = 0;
int angles[9] = {0, 0, 5, 90, 135, 180, 225, 270, 315};
byte lowstates = 0;

void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  esp_now_register_send_cb(OnDataSent);
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  Serial.println("booted up");
}

void loop() {
   for (int i = 0; i<9; i++){
    pinValues[i] = touchRead(theAnalogPins[i]);
    delay(5);
    newpinValues[i] = touchRead(theAnalogPins[i]);
    delay(5);
    newerpinValues[i] = touchRead(theAnalogPins[i]);
    averages[i] = ((pinValues[i] + newpinValues[i] + newerpinValues[i]) / 3);
    if (averages[i] < seuils[i]){
      state[i] = HIGH;
    } else {
      state[i] = LOW;
     }
   }

// process values
  lowstates = 0;
   for (int i = 0; i<9; i++){
    if ((state[i] == HIGH) and (i != 2)){
      angle = angles[i];
    } else {
      lowstates = lowstates + 1;
    }
    //Serial.print(state[i]);
    //Serial.print("\t");
  }
  Serial.println(" ");
  if ((state[1]) != HIGH){
    angle = 400;
  } 
  if (lowstates > 7) {
    angle = 400;
  }
  Serial.println(angle);
  struct_message myData;
  myData.a = angle;
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
   if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }
}
