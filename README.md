### UNIMOTION - Recherches autour de la possibilité de télécommander un fauteuil à assistance électrique.

## Ce projet a été réalisé dans le cadre du Crunch Maker Camp organisé par le Crunchlab de Belfort en 2021. Il vient se greffer a du matériel médical. Tous les objets partagés sur ce dépôt le sont a des fins de recherche et ne sont en aucun cas à reproduire dans une autre optique.

## L'ensemble du projet suivant ses composants est couvert par des licences libres à forte récopricité dont vous trouverez le détail ci-dessous.
- Partie composant carte électronique -> licence CERN-OHL-S
- Documentation + et modèle de la télécommande (fichiers 3D) -> CC-by-SA
- Code source du projet -> GNUGPL-v3

Fablab des Fabriques // 6 juin 2021

![](/doc/pics/DSC00063.JPG)
![](/doc/pics/DSC00070.JPG)
![](/doc/pics/DSC00071.JPG)

Ont participé à ce projet Olivier Laudy, Vincent Janousek, Colin Lussagnet, Georges Ladner, Patrice Lacroute, sous la houlette d'Olivier Testault et de Guillaume Bertrand.

Le point de départ du projet est la constatation que le déport des commandes de fauteuil électrique se font, de manière standardisée, sur les poignées des fauteuils roulant à assistance électrique. Cette posture n'offre pas à l'aidant une position spatiale favorisant une relation conversationnelle, lors d'une promenade, par exemple. Le projet a consisté à esquisser les briques matérielles et logicielles pour modifier de manière non destructive un jostick de marque PG Drives Technology, relativement standard, et pouvoir doubler le joystick classique de l'usager du fauteuil par un système télécommandé.

Une carte électronique vient s'intercaler entre la commande du joystick et la carte contrôleur du joystick, offrant la possibilité à deux ESP32 de contrôler le fauteuil à distance.  

En parallèle, une recherche autour de la réalisation de version renforcée de certaines pièces de fixation du moteur a été réalisée, avec des versions en aluminium et en laiton des pièces auparavant rélisée en impression 3D.

![](/doc/pics/DSC00026.JPG)
![](/doc/pics/DSC00034.JPG)
![](/doc/pics/DSC00038.JPG)
![](/doc/pics/DSC00056.JPG)
![](/doc/pics/DSC00058.JPG)
![](/doc/pics/DSC00060.JPG)

![](/doc/pics/DSC00052.JPG)
![](/doc/pics/DSC00018.JPG)
![](/doc/pics/DSC00020.JPG)
![](/doc/pics/DSC00023.JPG)
![](/doc/pics/DSC00024.JPG)
![](/doc/pics/DSC00028.JPG)
![](/doc/pics/DSC00029.JPG)
![](/doc/pics/DSC00042.JPG)

#### Déport des commandes de boutons, en PLA conducteur
![](/doc/pics/DSC00073.JPG)

#### Version des biellettes de fixation des moteurs en alu ou plexi + laiton pour recherches de résistance mécanique
![](/doc/pics/DSC00026.JPG)
![](/doc/pics/DSC00078.JPG)
![](/doc/pics/DSC00043.JPG)
![](/doc/pics/DSC00045.JPG)
![](/doc/pics/DSC00050.JPG)

#### Télécommande: 8 zones capacitives, en relief (comme du braille) pour piloter le fauteuil à distance
![](/doc/pics/DSC00081.JPG)

#### Prototypage électronique
![](/doc/pics/DSC00083.JPG)
![](/doc/pics/DSC00084.JPG)
![](/doc/pics/Flatcam.PNG)
![](/doc/pics/Kicad.PNG)
